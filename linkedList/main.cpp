#include <iostream>
#include<vector>
#include <bits/stdc++.h>
using namespace std;
struct node
{
    int data;
    node *next;
};
//
class linked_list
{
public:
    node *head,*tail;
public:
    linked_list()//default cons
    {
        head = NULL;
        tail = NULL;
    }
//
void add_node(int number){ //1 2
    node *tmp=new node;
    tmp->data=number;
    tmp->next=NULL;
    if(head==NULL||tail==NULL){
    head=tmp;
    tail=tmp;
    }
    else{
        tail->next=tmp;
        tail=tail->next;
    }
    //

}
void display(){
    node *tmp2=new node;
    tmp2=head;
    while(tmp2!=NULL){
        cout<<tmp2->data<<" ";
        tmp2=tmp2->next;
    }
    cout<<endl;
}
//
int maxone() {
        int length = 1;
        node *tmp2=new node;
        tmp2=head;
        while(tmp2 != NULL) {
            length *=2;
            tmp2 = tmp2->next;
        }

        return length;
    }
int getDecimalValue() {
    int sum=0;
     node *tmp2=new node;
    tmp2=head;
    int i=maxone()/2;

    while(tmp2!=NULL){

       sum+=tmp2->data*i;
        tmp2=tmp2->next;
        i/=2;
    }
    cout<<sum;
}
////////////////////
void reverseList(){
vector<int>holdData;
  node *tmp=new node;
  tmp=head;
  while(tmp!=NULL){
    holdData.push_back(tmp->data);
    tmp=tmp->next;
  }
    node *tmp2=new node;
  tmp2=head;
  for(int r=holdData.size()-1;r>=0;r--){
    tmp2->data=holdData[r];
   // cout<<tmp2->data;
       tmp2=tmp2->next;
  }

}
//////////////////////////////////
void deleteNodelast(){
    //Last Element
    node *tmp=new node;
    node *pre=new node;
    tmp=head;
    while(tmp->next!=NULL){
        pre=tmp;
        tmp=tmp->next;
    }
   tail=pre;
   pre->next=NULL;
   delete tmp;

}
//////////////////
void deleteNodeFirst(){
    //First Element
    node *tmp=new node;
    tmp=head;
    head=tmp->next;
    delete tmp;

}
////////////////////////////
void deleteFromAnyWhere(int element){// 1 2 3
    node *tmp=new node;
    node *pre=new node;
    tmp=head;
    for(int i=0;i<element-1;i++){
    pre=tmp;
    tmp=tmp->next;
    }
    pre->next=tmp->next;

}
////////////////////////////
int length() {
        int length = 0;
        node *tmp2=new node;
        tmp2=head;
        while(tmp2 != NULL) {
            length++;
            tmp2 =tmp2->next;
        }

        return length;
    }
//////////////////////////////
void DeleteNode(int element){
node *tmp=new node;
tmp=head;
int index=0;
while(element!=tmp->data){
  index++;
  tmp=tmp->next;
}
//cout<<index+1;
if(index==0){
    //start
deleteNodeFirst();
}
else if(index==length()-1){
    //end
    deleteNodelast();
}
else{
    //any one else
    deleteFromAnyWhere(index+1);
}

}
//////////////////////
int oddEvenList(){
    vector<int>vec;
    node *tmp=new node;
    tmp=head;
    while(tmp!=NULL){
        if(tmp->data%2!=0)
            vec.push_back(tmp->data);
        tmp=tmp->next;
    }
    //
    node *tmp2=new node;
    tmp2=head;
    //
      while(tmp2!=NULL){
        if(tmp2->data%2==0)
            vec.push_back(tmp2->data);
        tmp2=tmp2->next;
    }
    //
    node *tmp3=new node;
    tmp3=head;
for(int r=0;r<vec.size();r++){
    tmp3->data=vec[r];
    tmp3=tmp3->next;
}


}
////////////////////////
void maxElementLinkedList(){

node *temp=new node;
temp=head;
int maxvalue=temp->data;
while(temp!=NULL){//1 2 3
    if((temp->data)>maxvalue){
        maxvalue=temp->data;
    }
       temp=temp->next;
}

cout<<maxvalue;
}
///////////////////////
void displayUsingRecurison(node *next){
  if(next==NULL){
    return;
  }
cout<<next->data;
displayUsingRecurison(next->next);
}
///////////////////////
int counts(node *temp){
static int c=0;
if(temp==NULL){
    return 0; //like break
}
else{
    c++;
    counts(temp->next);
}
return c;
}
//////////////////////
int maxxs(node *temp){
   static int x=-7000;
if(temp->next==NULL){
    return 0 ;
}
x=max(x,temp->data);
//cout<<x;
maxxs(temp->next);
return x;

}
bool LinearSearch(int val){
node *temp=new node();
temp=head;
int counter=0;
while (temp!=NULL){
    if(val==temp->data){

       counter++;
       break;
    }

    temp=temp->next;
}
if(counter>=1){
    return true;
}
else{
    return false;
}

}
int linearSearchRecursive(node*temp,int val){
if(temp==NULL){
    return NULL;
}
else if(val==temp->data){
    return val;
}
else{
    linearSearchRecursive(temp->next,val);
}

}
///////////////////////////////
int add_At_position(int value,int pos){//add in all position first end any place :)
node *temp=new node;
node *pre=new node;
temp=head;
//
node *add=new node;
add->data=value;
add->next=NULL;
if(pos==1){
        //first
    add->next=temp;
    head=add;
}
else if(pos==length()){
    //end
    while(temp->next!=NULL){
        temp=temp->next;
       // cout<<temp->data<<" ";
    }
    //cout<<temp->data;
    temp->next=add;
}
else{

for (int r=0;r<pos-1;r++){
    pre=temp;
    temp=temp->next;
}
//cout<<pre->data<<"  "<<temp->data;
pre->next=add;
add->next=temp;
}

}
//////////////////////
void insert_in_sorted_list(int element){
node *temp=new node;
node *pre=new node;
node *creation=new node;
creation->data=element;
creation->next=NULL;
temp=head;
while(element>temp->data){
    pre=temp;
    temp=temp->next;
}
//cout<<pre->data<<"  "<<temp->data;
pre->next=creation;
creation->next=temp;
}
//////////////////////////
void check_sorted(){
node *ptr=new node;
ptr=head;
int counter=0;
while(ptr->next!=NULL&&(ptr->next->data)>(ptr->data)){
    counter++;
    ptr=ptr->next;

}
if(counter+1==length()){
    cout<<"SORTED";
}
else{
    cout<<"NOT SORTED";
}

}
//////////////
void removeDublicatedSortedList(){
node *ptr=new node;
node *prev=new node;
//
ptr=head;
prev=head;
//
while(ptr->next!=NULL){

    if(prev->data==ptr->data){
        prev->next=ptr->next;
        delete ptr;
        ptr=prev->next;
    }
    else{
            prev=ptr;
    ptr=ptr->next;
    }
}

}
///////////////
void add(int number){ //1 2
    node *tmp=new node;
    tmp->data=number;
    tmp->next=NULL;
    if(head==NULL||tail==NULL){
    head=tmp;
    tail=tmp;
    }
    else{
        tail->next=tmp;
        tail=tail->next;
    }
    //

}
//
void mergeLists(node *l1,node*l2){
node *ptr1=new node;
node *ptr2=new node;
vector<int>vec;
ptr1=l1;
ptr2=l2;
while(ptr1!=NULL){
    vec.push_back(ptr1->data);
    //cout<<ptr1->data<<" ";
    ptr1=ptr1->next;
}
while(ptr2!=NULL){
    vec.push_back(ptr2->data);
        //cout<<ptr2->data;
      ptr2=ptr2->next;
}
sort(vec.begin(),vec.end());
node *ptr3=new node;
for(int r=0;r<vec.size();r++){
 //add(vec[r]);
 cout<<vec[r]<<" ";
}

}
//
void concatinate (node *l1,node*l2){
while (l1->next!=NULL){
    l1=l1->next;
}
l1->next=l2;

}
//

};



int main()
{
    //1,0,0,1,0,0,1,1,1,0,0,0,0,0,0
    linked_list a,b;
    //a.add_node(400);
    //a.add_node(5);
    //a.add_node(50);
    //a.add_node(10);
    //a.add_node(2);
   //a.add_At_position(100,1);
    //a.add_At_position(1,1);
    //a.add_At_position(2,2);
    //a.add_At_position(5,3);
    //a.add_At_position(5,4);
    // a.add_At_position(5,5);
    //a.add_At_position(8,5);
   // cout<< a.loop();
      // a.removeDublicatedSortedList();
      //a.display();
       //a.insert_in_sorted_list(4);
      //a.check_sorted();
    //a.display();

    //node *temp=new node;
    //temp=a.head;//here we use head public not private
   //cout<<a.linearSearchRecursive(temp,0);
    //cout<<a.LinearSearch(0);
   // cout<<a.maxxs(temp);
   // a.displayUsingRecurison(temp);
  //cout<< a.counts(temp);
    //a.maxElementLinkedList();
    //a.oddEvenList();
    //a.reverseList();
    //a.deleteNodelast();
    //a.deleteNodeFirst();
    //a.deleteFromAnyWhere(4);
    //a.deleteFromAnyWhere(2);
    //a.deleteFromAnyWhere(5);
    //a.deleteFromAnyWhere(1);
    //a.display();
    //a.getDecimalValue();
   /* a.DeleteNode(4);
    a.display();
    a.DeleteNode(2);
    a.display();
      a.DeleteNode(1);
    a.display();
        a.DeleteNode(5);
    a.display();
        a.DeleteNode(3);*/
    //a.display();
      /*a.add_node(400);
      a.add_node(400);
      a.add_node(400);
      a.add_node(5);
      a.add_node(5);
      a.add_node(5);
      a.add_node(50);
      a.add_node(10);
      a.add_node(2);
      a.removeDublicatedSortedList();
      a.display();*/
      //node *temp=new node;
      //temp=a.head;//here we use head public not private
     // temp->next=a.tail;
    //cout<<  a.loop();
    //a.add(1);
    //a.add(2);
    //a.add(3);
    //node *temp=new node;
    //temp=a.head;//here we use head public not private
    //
    //b.add(4);
    //b.add(5);
    //b.add(6);
    //node *temp2=new node;
    //temp2=b.head;//here we use head public not private
    //
   //a.concatinate(temp,temp2);
  // a.display();
   // a.display();
    //b.display();
    /////////////////////////////////






    return 0;
}
